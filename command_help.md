# GOOD_ROBOT_LZG Help File

### 说明：() 代表用户自定义内容
***使用宽一点的屏幕，页面更美观哦~***

## 命令

<table>
<thead>
<th>功能</th>
<th>命令</th>
<th>参数1</th>
<th>参数2</th>
<th>参数3</th>
</thead>
<tbody>
<tr>
<td>是否开启未知请求警告</td>
<td>cmd\$unknown_request_warning</td>
<td>开启(t/true) 关闭(f/false)</td>
<td></td>
<td></td>
</tr>
<tr>
<td>添加文本回复 (方式一)</td>
<td>cmd\$addref\$()\$()</td>
<td>触发回复消息</td>
<td>回复消息</td>
<td></td>
</tr>
<tr>
<td>添加文本回复 (方式二)</td>
<td>cmd\$ar\$()</td>
<td>触发回复消息</td>
<td>回复消息</td>
<td></td>
</tr>
<tr>
<td>删除文本回复 (方式一)</td>
<td>cmd\$delref\$()</td>
<td>需要删除的触发回复消息</td>
<td></td>
<td></td>
</tr>
<tr>
<td>删除文本回复 (方式二)</td>
<td>cmd\$dr\$()</td>
<td>需要删除的触发回复消息</td>
<td></td>
<td></td>
</tr>
<tr>
<td>查看文本回复</td>
<td>cmd\$ref</td>
<td></td>
<td></td>
<td></td>
</tr>
<tr>
<td>运行cmd命令</td>
<td>cmd\$runservercommand\$()\$()</td>
<td>执行cmd命令的秘钥</td>
<td>cmd命令</td>
<td></td>
</tr>
<tr>
<td>获取详细服务器信息</td>
<td>cmd\$systeminfo</td>
<td></td>
<td></td>
<td></td>
</tr>
<tr>
<td>获取简要服务器信息</td>
<td>cmd\$minisysteminfo</td>
<td></td>
<td></td>
<td></td>
</tr>
<tr>
<td>封禁用户</td>
<td>cmd\$ban\$()</td>
<td>需要封禁的用户</td>
<td></td>
<td></td>
</tr>
<tr>
<td>解除封禁用户</td>
<td>cmd\$pardon\$()</td>
<td>需要解除封禁的用户</td>
<td></td>
<td></td>
</tr>
<tr>
<td>查看被封禁用户 (方式一)</td>
<td>cmd\$banned_list</td>
<td></td>
<td></td>
<td></td>
</tr>
<tr>
<td>查看被封禁用户 (方式二)</td>
<td>cmd\$bl</td>
<td></td>
<td></td>
<td></td>
</tr>
<tr>
<td>消息轰炸 (慎用)</td>
<td>cmd\$bomb\$()</td>
<td>轰炸次数</td>
<td></td>
<td></td>
</tr>
<tr>
<td>鬼叫 (鬼畜)</td>
<td>cmd\$guijiao\$()</td>
<td>生成鬼叫的文本</td>
<td></td>
<td></td>
</tr>
<tr>
<td>生草 (鬼畜)</td>
<td>cmd\$growgrass\$()\$()</td>
<td>生草模板, 使用 "n" "v" 代替名词 动词</td>
<td>生成次数</td>
<td></td>
</tr>
<tr>
<td>根据mediaId获取文件</td>
<td>cmd\$get_f_from_c\$()\$()\$()</td>
<td>文件mediaId</td>
<td>文件类型 <a href="https://open.dingtalk.com/document/orgapp/upload-media-files#h2-xg2-l8o-066">查看详情</a> </td>
<td>文件拓展名</td>
</tr>
<tr>
<td>封禁IP</td>
<td>cmd\$banip\$()</td>
<td>需要封禁的IP</td>
<td></td>
<td></td>
</tr>
<tr>
<td>解除封禁IP</td>
<td>cmd\$pardonip\$()</td>
<td>需要解除封禁的IP</td>
<td></td>
<td></td>
</tr>
<tr>
<td>查看被封禁的IP (方式一)</td>
<td>cmd\$banned_ip_list</td>
<td></td>
<td></td>
<td></td>
</tr>
<tr>
<td>查看被封禁的IP (方式二)</td>
<td>cmd\$bil</td>
<td></td>
<td></td>
<td></td>
</tr>
<tr>
<td>发送文本消息</td>
<td>cmd\$saytext\$()</td>
<td>需要发送的文本</td>
<td></td>
<td></td>
</tr>
<tr>
<td>发送Markdown消息</td>
<td>cmd\$saymd\$()</td>
<td>需要发送的Markdown</td>
<td></td>
<td></td>
</tr>
<tr>
<td>添加Markdown回复 (方式一)</td>
<td>cmd\$md\$addref\$()\$()</td>
<td>触发Markdown回复消息</td>
<td>回复Markdown消息</td>
<td></td>
</tr>
<tr>
<td>添加Markdown回复 (方式二)</td>
<td>cmd\$md\$ar\$()\$()</td>
<td></td>
<td></td>
<td></td>
</tr>
<tr>
<td>删除Markdown回复 (方式一)</td>
<td>cmd\$md\$delref\$()</td>
<td>需要删除的触发Markdown回复消息</td>
<td></td>
<td></td>
</tr>
<tr>
<td>删除Markdown回复 (方式二)</td>
<td>cmd\$md\$dr\$()</td>
<td></td>
<td></td>
<td></td>
</tr>
<tr>
<td>删除所有Markdown回复</td>
<td>cmd\$md\$delall</td>
<td></td>
<td></td>
<td></td>
</tr>
<tr>
<td>查看Markdown回复</td>
<td>cmd\$md\$ref</td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>
