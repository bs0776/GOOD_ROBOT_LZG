import requests
import json

def getAccessToken():
    AppKey = "dinge0juejdot80lwk3k"
    AppSecret = "NLXegfN6gDZNZIppuOWMlUnY_ibEPGZ_2pd87xtz9iBgGjja4lvu7XJmrJldY1i4"

    URL = "https://oapi.dingtalk.com/gettoken?appkey={}&appsecret={}".format(AppKey, AppSecret)

    req = requests.get(URL)

    data = json.loads(req.text)

    return data["access_token"]


def getMediaId(type: str, media: str):

    URL = "https://oapi.dingtalk.com/media/upload?access_token={}".format(getAccessToken())

    file = {
        "media" : (media, open(media, 'rb'))
    }

    data = {
        "type" : type
    }

    req = requests.post(url=URL, files=file, data=data)

    req_data = req.text

    return json.loads(req_data)["media_id"]