import string
import random
def random_string(length: int):
    letters, result = string.ascii_letters, ""

    for i in range(length):
        result += random.choice(letters)

    return result


if __name__ == "__main__":
    for i in range(10):
        print(random_string(10))