import random

def grow_grass(s: str):
    '''
    使用 "n", "v" 代替 名词, 动词
    输入示例: "n在n里v"
    '''

    l = list(s)
    o = ''

    # 生草词典
    n = ["奶奶", "金针菇", "奶奶办公室", "撤硕", "花园", "朱元璋", "秦始皇", "栗子头", "口味王"]
    v = ["焯", "创飞", "食", "鬼叫", "外翻", "憋笑", "吃石"]

    for i in l:
        if i == "n":
            o += random.choice(n)
        elif i == "v":
            o += random.choice(v)
        else:
            o += i

    return o

if __name__ == "__main__":
    print(grow_grass("n在n里v"))
